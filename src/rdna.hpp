#include <iostream>
#include <random>

using namespace std;


string randDNA(int seed, string b, int length){

	
  mt19937 engine(seed);
  int min = 1;
  int max = b.length();
  string output;
  
  std::uniform_int_distribution<> unifrm(min, max);
  
  for(int i = 1; i <= length; i++)
  {
	  switch(unifrm(engine))
	  {
		  case 1: output += b[0];
		  break;
		  case 2: output += b[1];
		  break;
		  case 3: output += b[2];
		  break;
		  case 4: output += b[3];
		  break;
		  case 5: output += b[4];
		  break;
		  case 6: output += b[5];
		  break;
		  case 7: output += b[6];
	  }
  }
		  
return output;
}
